#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load busco/5.2.2
module load MetaEuk/4.0

busco -c 12 -i wbp_3kb_filtered.fa -l embryophyta_odb10 -o busco_3kb_xeon -m genome 


echo -e "\nEnd time:"
date

