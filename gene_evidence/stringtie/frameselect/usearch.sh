#!/bin/bash
#SBATCH --job-name=usearch
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load usearch/9.0.2132
module load seqtk
org=/core/labs/Wegrzyn/whitebarkpine/stringtie/frameselect/stringtie_merge_transcripts.fa.transdecoder.cds
seqtk seq -L 300 $org > transdecoder_over_300bp.cds
usearch --threads 8 --cluster_fast transdecoder_over_300bp.cds --centroids centroids_stringtie_merge --uc centroids_stringtie_merge.uc --id 0.80


echo -e "\nEnd time:"
date

