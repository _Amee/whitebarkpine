#!/bin/bash
#SBATCH --job-name=repeatmask
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50GB
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[1-100]%20
##SBATCH --mail-type=ALL
##SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o repeat_mask_%x_%A_%a.out
#SBATCH -e repeat_mask_%x_%A_%a.err

date
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID

module load perl/5.28.1
export PATH=/core/labs/Wegrzyn/annotationtool/software/RepeatMasker/4.0.6:$PATH

splitfilename=/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/wbp_3kb_filtered.fa"$SLURM_ARRAY_TASK_ID".fa
library=/core/labs/Wegrzyn/whitebarkpine/repeats/wbp_new_dir-families.fa

RepeatMasker -pa 16 -gff -a -noisy -low -xsmall -lib $library $splitfilename 


echo $splitfilename was softmasked on $(date) 




