[[_TOC]] 
# Whitebark Pine Genome Annotation

# Introduction

**The purpose of this git is to document the process of annotating the whitebark pine (*Pinus albicaulis*)**

The whitebark pine (WBP) is a high-elevation pine native to the mountain ranges of the western US and parts of Canada. It serves as a shelter and an important food source for many animals, especially the Clark's nutracker. WBP has been classified as endangered by the IUCN and in Canada. Most recently, the US Fish and Wildlife service announced that WBP would be listed as threatened under the Endangered Species Act. The decline of WBP is due to a combination of climate change and increasing wildfires, pests such as the mountain pine beetle, and White Pine Blister Rust (WPBR), an invasive fungal pathogen affecting all North American white pines. FIrst introduced in the early 1900s, WPBR has spread throughout much of the native range of the white pine species. Sequencing, assembling, and annotating conifer genomes, especially for pines, is a major challenge due to their large and highly repetitive genomes. Genomes exist for only two other pine species, P. taeda and P. lambertiana, which is also another white pine).

![alt text](imgs/whitebarkpinekrumholtz.jpeg "high elevation wbp"){width=300px height=400px} ![alt text](imgs/WhitebarkRange.jpeg "native range of wbp"){width=300px height=400px} 
![alt text](imgs/clarksnutcrackeronwbp.jpeg "clark's nutcracker on wbp"){width=400px height=300px} 


**People involved**

- Amanda De La Torre 
- Aleksey Zimin
- Jill Wegrzyn
- Richard Sniezko 
- Laura Figueroa Corona
- Maurice Amee
- Akriti Bhattarai


RNA seq libraries were downloaded from the NCBI SRA to assemble transcripts as evidence to use in genome annotation. Quality control results from fastp can be found in the [fastp_reports](/fastp_results) folder. Only libraries that were retained for the annotation are listed here, libraries with less than 95% mapping rate to the assembled genome were removed. The full table can be found [here](https://docs.google.com/spreadsheets/d/1QIbOfE1QlSzpqm9k73CVsCKTYp4z4zLB4WvkHy4E0yg/edit#gid=1297237946)

|SRA accessions| project     | tissue type      | pooled/single | raw reads (M) | trimmed reads (M) | mapping rate |
|------------- | ----------- | ---------------- | ------------- | ------------- | ----------------- | ------------ |
|SRR13823648   | PRJNA703422 | megagametophytes | single        | 68.861096     | 66.916378         | 95.2         |
|SRR5368574    | PRJNA352055 | Needle           | single        | 38.026650     | 37.162830         | 96           |
|SRR5368575    | PRJNA352055 | Needle           | single        | 41.248962     | 40.279086         | 95.9         |
|SRR5368576    | PRJNA352055 | Needle           | single        | 37.951822     | 37.951822         | 95.3         |
|SRR5368577    | PRJNA352055 | Needle           | single        | 35.176356     | 34.236232         | 95.3         |
|SRR5368578    | PRJNA352055 | Needle           | single        | 34.290692     | 33.466354         | 95.5         |
|SRR5368579    | PRJNA352055 | Needle           | single        | 32.297144     | 31.417736         | 96           |
|SRR5368580    | PRJNA352055 | Needle           | single        | 25.204748     | 24.604476         | 96           |
|SRR5368581    | PRJNA352055 | Needle           | single        | 30.622742     | 29.958086         | 96           |
|SRR5368582    | PRJNA352055 | Needle           | single        | 24.447170     | 23.868976         | 94.8         |
|SRR4786281    | PRJNA352055 | Needle           | single        | 36.080828     | 35.094860         | 95           |
|SRR4786284    | PRJNA352055 | Needle           | single        | 46.218548     | 44.478282         | 94.8         | 

**The path to the directory with the final alignments is: /core/labs/Wegrzyn/whitebarkpine/hisat2/alignments**

# 1. Genome statistics 

The genome was first filtered to remove contigs under 3 Kbp. 
 Full QUAST and BUSCO results can be found [here](https://docs.google.com/spreadsheets/d/1PRSOtwvo4R8uRRW227t6skiEeOJPsP3vFgLujpakhjo/edit#gid=0)

**The full path to the filtered genome is: /core/labs/Wegrzyn/whitebarkpine/data/wbp_3kb_filtered.fa** 

## BUSCO statistics

Using BUSCO version 5.2.2 with MetaEuk/4.0 on the embryophyta_odb10 lineage

**BUSCO was run in genome mode**
	
	***** Results: *****

	C:55.3%[S:45.5%,D:9.8%],F:24.3%,M:20.4%,n:1614	   
	893	Complete BUSCOs (C)			   
	735	Complete and single-copy BUSCOs (S)	   
	158	Complete and duplicated BUSCOs (D)	   
	393	Fragmented BUSCOs (F)			   
	328	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched	

|      | C:55.3%\[S:45.5%,D:9.8%\],F:24.3%,M:20.4%,n:1614 |
| ---- | ------------------------------------------------ |
| 893  | Complete BUSCOs (C)                              |
| 735  | Complete and single-copy BUSCOs (S)              |
| 158  | Complete and duplicated BUSCOs (D)               |
| 393  | Fragmented BUSCOs (F)                            |
| 328  | Missing BUSCOs (M)                               |
| 1614 | Total BUSCO groups searched                      |


As a reference, here are BUSCO results from selected plant species. Two other pine species have been seqeunced, *P. lambertiana* and *P. taeda*. We can see that at 55%, the BUSCO completness is higher for the whitebark pine genome than the other two pine genomes. 

![alt text](imgs/busco_of_selected_species.png "table of busco statistics for selected plant species"){width=600 height=375px}


## QUAST statistics

| Assembly                   | wbp\_3kb\_filtered |
| -------------------------- | ------------------ |
| \# contigs (>= 0 bp)       | 95547              |
| \# contigs (>= 1000 bp)    | 95547              |
| \# contigs (>= 5000 bp)    | 87199              |
| \# contigs (>= 10000 bp)   | 79850              |
| \# contigs (>= 25000 bp)   | 72363              |
| \# contigs (>= 50000 bp)   | 63925              |
| Total length (>= 0 bp)     | 27675239795        |
| Total length (>= 1000 bp)  | 27675239795        |
| Total length (>= 5000 bp)  | 27642734473        |
| Total length (>= 10000 bp) | 27590743631        |
| Total length (>= 25000 bp) | 27464215223        |
| Total length (>= 50000 bp) | 27153608645        |
| \# contigs                 | 95547              |
| Largest contig             | 7156012            |
| Total length               | 27675239795        |
| GC (%)                     | 37.15              |
| N50                        | 736010             |
| N90                        | 162777             |
| auN                        | 950726.7           |
| L50                        | 10789              |
| L90                        | 41110              |
| \# N's per 100 kbp         | 0.24               |

## Sample Scripts

### a. BUSCO
```
#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load busco/5.2.2
module load MetaEuk/4.0
busco -c 12 -i wbp_3kb_filtered.fa -l embryophyta_odb10 -o busco_3kb_xeon -m genome 
date
```

### b. QUAST
```
#!/bin/bash
#SBATCH --job-name=quast2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load quast/5.2.0
quast.py -t 12 -o quast3kb wbp_3kb_filtered.fa
date
```

# 2. Repeat Masking

The first step is to softmask repetitive regions of the genome. 
This is done using RepeatModeler2 and RepeatMasker. 

First, RepeatModeler2 is used to generate custom libraries of repeat elements from the genome. Version 2 of RepeatModeler also includes an additional LTR structural identification pipeline using LTRharvest and LTRretriever. In our case, there were difficulties using the LTR structural pipeline. Therefore, we ran RepeatMasker by two methods. The first uses only the repeat libraries generated through RepeatModeler without LTRs. The second method uses sequential runs of RepeatMasker addding in LTR analysis results in the second round of masking. The genome was split into 100 sections to speed up the masking process. The scripts can be found in the repeats folder. 

| Total number of models from RepeatModeler2 | known models | unknown models | 
| ------------------------------------------ | ------------ | -------------- |
| 2576                                       | 463          | 2113           |

| # of masked bases | Total # of bases  | % genome masked | 
| ----------------- | ----------------- | --------------- |
| 16321422485       | 27675239795       | 58.975          |


The models from RepeatModeler were run with TEsorter to further identify the type of repeat
| Repeat    | \# of sequences |
| --------- | --------------- |
| LTR Copia | 152             |
| LTR Gypsy | 347             |
| Other LTR | 1               |
| LINE      | 25              |
| TIR       | 16              |
| Other     | 17              |
| total     | 558             |

**The full path to the repeatmasking process can be found in:** /core/labs/Wegrzyn/whitebarkpine/repeats

**The full path to the softmasked genome is:** /core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/double_masked_wbp.fa

## Sample Scripts

### a. RepeatModeler2
```
#!/bin/bash
#SBATCH --job-name=repeatmodel_new_tmp
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/
module load RepeatModeler/2.01
module load genometools/1.6.1
module load mafft/7.471
export LTRRETRIEVER_PATH=/core/labs/Wegrzyn/annotationtool/software/LTR_retriever
module load cdhit/4.8.1
module load ninja/0.95
export TMPDIR=/core/labs/Wegrzyn/whitebarkpine/
BuildDatabase -name wbp_highermem /core/labs/Wegrzyn/whitebarkpine/data/wbp_3kb_filtered.fa
RepeatModeler -database wbp_highermem -pa 12 -LTRStruct
#RepeatModeler/RepeatClassifier -consensi /core/labs/Wegrzyn/whitebarkpine/repeats/RM_1302892.TueDec201422112022/consensi.fa.classified
source activate tesorter
TEsorter -st nucl -pre wbp_te_default_db -p 12 -tmp $HOME/tmp /core/labs/Wegrzyn/whitebarkpine/repeats/wbp_highermem-families.fa
date
```

### b. Repeatmasker step 1 - split genome
```
#!/bin/bash
#SBATCH --job-name=onlytop
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
#activate base python env 
source activate
python ./split3.py
date
```

**split3.py**
```
#!/usr/bin/python

from Bio import SeqIO
import argparse
#import numpy as np
import os

pieces=100

filename=open("/core/labs/Wegrzyn/whitebarkpine/data/wbp_3kb_filtered.fa", 'r')
seq_records1 = SeqIO.parse(filename, "fasta")
seq_records1 = list(seq_records1)

print(seq_records1)

# Calulate total number of bases

genome=0

for seq_record in seq_records1:
   genome+=len(seq_record.seq)

splice=genome/pieces  
j=0 
```

### c. Repeatmasker step 2 - mask first round with original RM2 library
```
#!/bin/bash
#SBATCH --job-name=repeatmask
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50GB
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[1-100]%20
##SBATCH --mail-type=ALL
##SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o repeat_mask_%x_%A_%a.out
#SBATCH -e repeat_mask_%x_%A_%a.err
date
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
module load perl/5.28.1
export PATH=/core/labs/Wegrzyn/annotationtool/software/RepeatMasker/4.0.6:$PATH
splitfilename=/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/wbp_3kb_filtered.fa"$SLURM_ARRAY_TASK_ID".fa
library=/core/labs/Wegrzyn/whitebarkpine/repeats/wbp_new_dir-families.fa
RepeatMasker -pa 16 -gff -a -noisy -low -xsmall -lib $library $splitfilename 
echo $splitfilename was softmasked on $(date) 
```

### d. Repeatmasker step 3 - mask second round with additional LTRs
```
#!/bin/bash
#SBATCH --job-name=repeatmask
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50GB
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[1-100]%20
##SBATCH --mail-type=ALL
##SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o repeat_mask_%x_%A_%a.out
#SBATCH -e repeat_mask_%x_%A_%a.err
date
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
module load perl/5.28.1
export PATH=/core/labs/Wegrzyn/annotationtool/software/RepeatMasker/4.0.6:$PATH
splitfilename=/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/wbp_3kb_filtered.fa"$SLURM_ARRAY_TASK_ID".fa
library=/core/labs/Wegrzyn/whitebarkpine/repeats/wbp_new_dir-families.fa
RepeatMasker -pa 16 -gff -a -noisy -low -xsmall -lib $library $splitfilename 
echo $splitfilename was softmasked on $(date) 
```

# 3. Preliminary Annotation with Stringtie

The hisat2 alignments of each of SRA accessions to the genome were passed to Stringtie2 for genome-guided transcriptome assembly. Stringtie2 merge was then used to generate one final reference transcriptome from the multiple assemblies. GFFread was used to extract the transcripts using the output of Stringtie2 merge.

Transdecoder with eggnog was used for frame selection and translation to peptide sequences. USEARCH was used to cluster the transcripts at 0.80 identity, however, this reduced the busco completeness of the transcripts, and the unclustered transcripts were retained. There were 51,844 transcripts total.

## QUAST assessments of transcriptome

| transcripts | N50   | 
| ----------  | ----- | 
| 51844       |  1566 |            

## BUSCO assessments of gene evidence

Using BUSCO version 5.2.2 and the embryophyta_odb10 lineage 

**BUSCO was run in mode: proteins**

	***** Results: *****

	C:70.9%[S:43.3%,D:27.6%],F:15.2%,M:13.9%,n:1614	   
	1145	Complete BUSCOs (C)			   
	699	Complete and single-copy BUSCOs (S)	   
	446	Complete and duplicated BUSCOs (D)	   
	245	Fragmented BUSCOs (F)			   
	224	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		

|      | C:70.7%[S:43.4%,D:27.3%],F:15.1%,M:14.2%,n:1614 |
| ---- | ----------------------------------------------- |
| 1141 | Complete BUSCOs (C)                             |
| 700  | Complete and single-copy BUSCOs (S)             |
| 441  | Complete and duplicated BUSCOs (D)              |
| 243  | Fragmented BUSCOs (F)                           |
| 230  | Missing BUSCOs (M)                              |
| 1614 | Total BUSCO groups searched                     |

## Sample Scripts 

### a. hisat2 create database and alignment
```
#!/bin/bash
#SBATCH --job-name=indexbuild_3kb 
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load hisat2/2.2.1

hisat2-build -p 12 ../data/wbp_3kb_filtered.fa wbp_3kb_filt 
date
```

```
#!/bin/bash
#SBATCH --job-name=hisatalign_SRR13823648
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load hisat2
module load samtools

mkdir -p alignments
hisat2 -p 12 -x wbp_3kb_filt --max-intronlen 2500000 -1 ../fastp/trimmed_SRR13823648_1.fastq.gz -2 ../fastp/trimmed_SRR13823648_2.fastq.gz -S SRR13823648.sam
samtools view -b -@ 12 SRR13823648.sam | samtools sort -o sorted_SRR13823648.bam -@ 12
rm SRR13823648.sam
date
```

### b. Stringtie
```
#!/bin/bash
#SBATCH --job-name=stringtie
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname 
date
module load stringtie/2.2.1
stringtie -o SRR13823648_stringtie.gtf -l wbp -p 8 /core/labs/Wegrzyn/whitebarkpine/hisat2/sorted_SRR13823648.bam
date
```

### c. Stringtie merge
```
#!/bin/bash
#SBATCH --job-name=mergestringtie
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load stringtie/2.2.1
ls -1 *.gtf >> sample_assembly_gtf_list.txt
stringtie --merge -p 8 -o whitebark_stringtie_merged.gtf sample_assembly_gtf_list.txt  
date
```

### d. Get fasta file 
```
#!/bin/bash
#SBATCH --job-name=get_fa_from_gtf
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load gffread/0.12.1
org=/core/labs/Wegrzyn/whitebarkpine/data/
#extract transcript sequences from annotation file
gffread -w stringtie_merge_transcripts.fa -g $org/wbp_3kb_filtered.fa whitebark_stringtie_merged.gtf
date
```

### e. Frame selection using Transdecoder and EggNOG
```
#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date
module load sqlite
module load hmmer/3.3.2
module load perl/5.24.0
module load TransDecoder/5.5.0
module load blast/2.11.0
source activate eggnog
filename=stringtie_merge_transcripts.fa
#TransDecoder.LongOrfs -t $filename
emapper.py -i $filename.transdecoder_dir/longest_orfs.pep -o eggnog.blastp -m diamond --cpu 16
TransDecoder.Predict -t $filename --retain_blastp_hits eggnog.blastp.emapper.hits
echo -e "\nEnd time:"
date
```

## 3.5. Annotation with BRAKER

There are two main outputs from braker that we used, the braker.gtf file and the augustus.hints.gtf/augustus.hints.aa outputs. The difference between braker.gtf and augusuts.hints.gtf is that the braker.gtf is the combination of predicted unqiue genes from all methods braker uses to predict genes, while the augustus file only has the predicted genes coming out of augustus. 

From the output braker.gtf file, the transcripts were extracted from the genome to run BUSCO. There were a total of 640806 "genes" identified by braker

### BUSCO was run in mode: transcriptome**

	***** Results: *****

	C:44.0%[S:32.9%,D:11.1%],F:31.3%,M:24.7%,n:1614	   
	710	Complete BUSCOs (C)			   
	531	Complete and single-copy BUSCOs (S)	   
	179	Complete and duplicated BUSCOs (D)	   
	505	Fragmented BUSCOs (F)			   
	399	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   


The augustus.hints.aa was also ran through BUSCO. The augustus.hints.aa file contained the translated protein sequences of the coding sequences output by augustus. Th

There were a total of 636628 "genes" identified with just Augustus, as the braker output is a combination of the Augustus and MetaEuk results.

**BUSCO was run in mode: proteins**

	***** Results: *****

	C:45.0%[S:35.6%,D:9.4%],F:30.8%,M:24.2%,n:1614	   
	727	Complete BUSCOs (C)			   
	575	Complete and single-copy BUSCOs (S)	   
	152	Complete and duplicated BUSCOs (D)	   
	497	Fragmented BUSCOs (F)			   
	390	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   


### Combining stringtie2 and braker outputs

The frame selected proteins from stringtie2 and the braker augustus output were combined together to assess if this would result in a more complete annotation. Combining all of the 636,628 genes from augustus.hints.aa with the stringtie output did slightly increase the BUSCO completeness compared to just stringtie (73.9% vs 70.7%), however, it also greatly increased the duplication. The augustus.hints.aa proteins were also filtered based on if they had an eggnog match/annotation using eggnog mapper, and this did not affect the BUSCO scores. It did decrease the total number of predicted genes (in the combined set) from 688,472 to 271,318.

**BUSCO was run in mode: proteins**

	***** Results: *****

	C:73.9%[S:23.6%,D:50.3%],F:15.7%,M:10.4%,n:1614	   
	1193	Complete BUSCOs (C)			   
	381	Complete and single-copy BUSCOs (S)	   
	812	Complete and duplicated BUSCOs (D)	   
	253	Fragmented BUSCOs (F)			   
	168	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		  


## Sample Scripts

### braker
```
#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
org=/core/labs/Wegrzyn/whitebarkpine
mkdir -p /core/labs/Wegrzyn/whitebarkpine/braker/tmp
module load python/3.6.3
module load biopython/1.70
module load perl/5.28.1
module load bamtools/2.5.1
module load blast/2.10.0
module load genomethreader/1.7.1
export AUGUSTUS_CONFIG_PATH=$HOME/personal_software/Augustus_3.3.3/config/
export AUGUSTUS_BIN_PATH=$HOME/personal_software/Augustus_3.3.3/bin
export PATH=$HOME/personal_software/BRAKER_2.1.5:$HOME/personal_software/BRAKER_2.1.5/scripts:$PATH
export TMPDIR=/core/labs/Wegrzyn/whitebarkpine/braker/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=$HOME/personal_software/cdbfasta
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export GENEMARK_PATH=/core/labs/Wegrzyn/annotationtool/software/gmes_linux_64
species='whitebarkpine'
genome=/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/masked_wbp_old_rm.fa
bam1=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR13823648.bam
bam2=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR4786281.bam
bam3=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR4786284.bam
bam4=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368574.bam
bam5=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368575.bam
bam6=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368576.bam
bam7=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368577.bam
bam8=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368578.bam
bam9=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368579.bam
bam10=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368580.bam
bam11=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368581.bam
bam12=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368582.bam
if [[ -d $AUGUSTUS_CONFIG_PATH/species/$species ]]; then rm -r $AUGUSTUS_CONFIG_PATH/species/$species; fi
#cp ~/local_gm_key ~/.gm_key
#cp ~/local_gm_key $homedir/.gm_key
braker.pl --cores 20 --genome="$genome" --species="$species" --bam="$bam1,$bam2,$bam3,$bam4,$bam5,$bam6,$bam7,$bam8,$bam9,$bam10,$bam11,$bam12" --softmasking 1 --gff3
date
```


### Summary of the various annotations, for more details on the methods, see [annotation_method.md](link to file)

This table is a summary of the main annotations: The final selected annotation is highlighted in bold

| Run                                                      | Total Transcripts | rnaQUAST N50 | QUAST N50 | BUSCO                                        | mono/multi ratio | EnTAP similarity search annotation rate |
| -------------------------------------------------------- | ----------------- | ------------ | --------- | -------------------------------------------- | ---------------- | --------------------------------------- |
| Stringtie2 (SR) Full Set (6 libs)                        | 63123             | 1529         | 2281      | C:70.9%[S:43.3%,D:27.6%],F:15.2%,M:13.9%     | N/A              | N/A                                     |
| Stringtie2 (SR) Full: EggNOG Filtered Set                | 51844             | 1095         | 1566      | C:70.7%[S:43.4%,D:27.3%],F:15.1%,M:14.2%     | N/A              | 0.8943947226                            |
| BRAKER (SR RNA): Full                                    | 636628            | 450          | 963       | C:45.0%[S:35.6%,D:9.4%],F:30.8%,M:24.2%      | N/A              | N/A                                     |
| BRAKER (RNA) Full: EggNOG Filtered                       | 219474            | 1800         | 1164      | C:45.0%[S:35.7%,D:9.3%],F:30.9%,M:24.1%      | 0.6813917542     | N/A                                     |
| Stringtie2 (SR Full + LR transcripts)                    | 62936             | 785          | 1807      | C:71.4%[S:37.4%,D:34.0%],F:16.5%,M:12.1%     | N/A              | N/A                                      |
| Stringtie2 (SR Full + LR transcripts): EggNog Filtered   | 47132             | 1200         | 1509      | C:70.4%[S:45.7%,D:24.7%],F:16.0%,M:13.6      | N/A              | 88.9                                    |
| **ST2 (SR Full) eggnog and gff3 filtered**               | **48567**         | N/A          | **1578**  | **C:70.5%[S:43.2%,D:27.3%],F:15.1%,M:14.4%** | **0.1411419173**     | **0.8497539482**                            |
| ST 2 (SR Full + LR transcripts) eggnog and gff3 filtered | 45380             | N/A          | 1515      | C:70.3%[S:45.6%,D:24.7%],F:16.0%,M:13.7%     | 0.1742786906     | 0.8081092993                            |

**the full tables can be found [here](https://docs.google.com/spreadsheets/d/1QIbOfE1QlSzpqm9k73CVsCKTYp4z4zLB4WvkHy4E0yg/edit#gid=0)**

Braker performed worse than Stringtie2 with a lower N50 and very incomplete annotation (based on BUSCO scores. Combining the Stringtie2 and Braker outputs did not provide a significant improvement in completeness, and was still over-predicting genes even after filtering with EggNOG. Balancing number of transcripts, EnTAP similarity search rate, BUSCO completeness, and transcript N50, the final selected annotation was the Stringtie2 run with the short read libraries with EggNOG filtering at the frameselection step. 

EnTAP was run with the plant RefSeq and UniProt databases for similarity search and only the PFAM database with InterProScan.

## Sample Scripts 

**The config file used with EnTAP is located [here](EnTAP/entap_config.ini)

## EnTAP 
```
#!/bin/bash
#SBATCH --job-name=entap_short
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
EnTAP --runP --ini entap_config.ini -i /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_short.pep -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.208.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16 --out-dir $PWD/entap_stringtie_short_plusuni 
date 
```

# 4. Final Selected Annotation 
## Final Files
Masked genome: /core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/double_masked_wbp.fa
Annotation: Stringtie Short Reads only, after frame selection with transdecoder and filtering transcripts without an eggnog hit, and then remapped to the genome using Transdecoder  
- gff: /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie.transdecoder.genome.gff3
- cds: /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_short.cds
- pep: /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_short.pep
- entap output: /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/entap_stringtie_short/final_results/final_annotations_lvl0.tsv

## Methods

# 5. Additional Assessments

# 6. NLR identification

## NLR annotator


**Sample script:**
```
#!/bin/bash
#SBATCH --job-name=test_whitebark
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
ndir=/home/FCAM/abhattarai/personal_software/nlr_annotator2/
java -jar $ndir/NLR-Annotator-v2.1b.jar -i /core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/double_masked_wbp.fa -x $ndir/mot.txt -y $ndir/store.txt -o test_wbp.txt -g test_wbp.gff -b test_wbp.bed -m test_wbp_motif.bed -a test_wbp_alignment.fasta -t 16 
date
```

### RGAugury

using the genome annotation:

| NBS| CNL | TNL | CN | TN | NL | total |
| -- | --- | --- | -- | -- | -- | ----- |
| 73 | 21  | 44  | 11 | 33 | 91 | 273   |

previously with transcriptome:

| NBS | CNL | TNL | NL | CN | TN | total |
| --  | --- | --- | -- | -- | -- | ----- |
| 27  | 3   | 14  | 24 | 2  | 11 | 81    |

**Sample script:**
```
#!/bin/bash
#SBATCH --job-name=rga_wbp
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --exclude=xanadu-[05,10,13,14,15,16,25,26,30,51,61,62]
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
date
module load rgaugury/2019_02_12
export PFAMDB=/home/FCAM/abhattarai/database/pfam
export PATH=$PATH:/isg/shared/apps/rgaugury/2019_02_12
export COILSDIR=/isg/shared/apps/rgaugury/2019_02_12
cut -d " " -f 1 /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_short.pep > nospace_wbp.pep.fa
perl /isg/shared/apps/rgaugury/2019_02_12/RGAugury.pl -p nospace_wbp.pep.fa -c 10
date
```
